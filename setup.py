import setuptools


with open('README.md', 'r') as fd:
    long_description = fd.read()


setuptools.setup(
    name='gitignore-tool',
    version='0.0.1',
    author='Mukund K',
    author_email='mukund.kri@gmail.com',
    description='Generates a .gitignore file for a new project',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/mukund-kri/gitingore-tool',
    package_dir={'': 'src'},
    packages=setuptools.find_packages(),

    scripts=[
        'scripts/gitignore.py',
    ],

    install_requires=[
        'click==7.1.2',
        'GitPython==3.1.11',
    ],

)
