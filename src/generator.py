from pathlib import Path


def generator(cache_dir, envs):

    with open('.gitignore', 'w') as outfd:
    
        for env in envs:
            env = env.capitalize()
            file = f"{env}.gitignore"
            path = cache_dir / file

            with open(path, 'r') as fd:
                outfd.write(fd.read())

    
