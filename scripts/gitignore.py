'''
Entry point to the gitignore tool.

Right now it has 2 commands. 
1. Cache - Pull the latest from the repo
2. Gen   - Generate gitignore file from 1 or more templates
'''
from pathlib import Path

import click

from cacher import cache_update
from generator import generator


CACHE_DIR = Path.home() / '.local' / 'gitignores'
MAIN_REPO = 'https://github.com/hackstreet-india/gitignore.git'


@click.group()
def gitingnore():
    ''' The core command. Does nothing for now'''
    pass


@gitingnore.command()
def cache():
    cache_update(MAIN_REPO, CACHE_DIR)


@gitingnore.command()
@click.argument('envs', nargs=-1)
def gen(envs):
    generator(CACHE_DIR, envs)


if __name__ == '__main__':
    # Run the cli app
    gitingnore()


